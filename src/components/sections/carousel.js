import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

import Carousel from 'nuka-carousel'

const StyledCarousel = styled(Carousel)`
  width: 100%;
`;

const carouselSlidesText = [
  {
    text: 'Qui non si mangia la pizza, ma si fa una vera e propria esperienza gastronomica. Per questo il locale di Patrick Ricci, geniale maestro delle lievitazioni e degli impasti, con farine scelte e miscelate personalmente, è ideale per chi non ha paura di lasciarsi stupire.',
    site: 'ilgolosario.it',
    link: 'https://www.ilgolosario.it/ristoranti/pomodorobasilico'
  },
  {
    text: "L'insegna che si legge non è un 'sottotitolo' tanto per dire: è un programma, una filosofia precisa. Quattro anni di lavoro per andare alla ricerca delle 257 tipologie di grano che conta il nostro paese. Oggi Patrick Ricci acquista piccoli appezzamenti direttamente dai contadini, dalla Sicilia all'Emilia, al Piemonte ...",
    site: 'gamberorosso.it',
    link: 'https://www.gamberorosso.it/ristoranti/scheda-pizzeria/ricci-terra-grani-esplorazioni/'
  },
  {
    text: "Patrick Ricci è davvero un gran personaggio, estroso, vulcanico, sempre alla ricerca di cose nuove, di nuovi esperimenti. E i risultati sono sempre eccellenti, e questo grazie alla sperimentazione continua di nuovi impasti… ",
    site: '50toppizza.it',
    link: 'https://www.50toppizza.it/pizzeria/2018/patrick-ricci-terra-grani-esplorazioni/'
  },
  {
    text: "“Pizza. Il talento assoluto di Patrick Ricci che destabilizza da Torino in giù.”​",
    site: 'Scattidigusto.it',
    link: 'https://www.scattidigusto.it/2018/03/13/pizzeria-torino-patrick-ricci/'
  },
  {
    text: 'Il locale ha ancora le sembianze di una pizzeria come tante, ma quando vi sedete e iniziate ad assaggiare la pizza che servono a spicchi rimarrete stupiti...',
    site: 'ilgolosario.it',
    link: 'https://www.ilgolosario.it/assaggi-e-news/ristoranti/san-mauro-torinese-patrick-ricci'
  },
  {
    text: "Da Patrick Ricci non si va a cena per il locale di design. Il patron è un uomo energico. E' una di quelle persone che o si amano o si odiano. Bianco o nero. Non ci sono mediazioni. Non ci sono vie di mezzo. Sicuramente sincero. Come le sue pizze. ...",
    site: 'identitagolose.it',
    link: 'https://www.identitagolose.it/sito/it/62/20589/ristoranti/patrick-ricci.html'
  }
]

export const CarouselSection = () => {  

	const data = useStaticQuery(graphql`
		query {
      allFile(filter: {extension: {regex: "/(jpg)|(png)|(jpeg)/"}, relativeDirectory: {eq: "carousel"}}) {
        edges {
          node {
            childImageSharp {
              fixed {
                src
              }
            }
          }
        }
      }
    }
  `);

	return (
		<div
      css={`
        height: '625px'
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        background-color: white;
        justify-content: center;
			`}
		>
      <StyledCarousel
        dragging={true}
        speed={1000}
        autoplay
        wrapAround={true}
        zoomScale={0.5}
        renderCenterLeftControls={({ previousSlide }) => (
          <button style={{fontSize: '60px', color: 'white', marginLeft: '10px'}} onClick={previousSlide}>
            {`<`}
          </button>
        )}
        renderCenterRightControls={({ nextSlide }) => (
          <button style={{fontSize: '60px', color: 'white', marginRight: '10px'}} onClick={nextSlide}>
            {`>`}
          </button>
        )}
      >
      { data &&
        data.allFile.edges.map((el, i) => (
          <div
            style={
              {
                height: '625px',
                backgroundImage: `linear-gradient(to bottom, rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url(${el.node.childImageSharp.fixed.src})`,
                backgroundSize: 'cover',
                display: 'flex',
                alignItems: 'center',
                justifyContent: 'center'
              }
            }
          >
              <div
                css={`
                  width: 500px;
                  font-size: 26px;
                  color: white;
                  line-height: 1.4em;
                  font-family: libre baskerville, serif;
                  @media (max-width: 1000px) {
                    width: 60%;
                    font-size: 15px;
                  }
                `
                }
              >
                {carouselSlidesText[i].text}
                <div style={{margin: '50px 0 0 30px', textDecoration: 'underline', fontStyle: 'oblique', fontSize: '20px'}}>
                  <a href={carouselSlidesText[i].link}>- {carouselSlidesText[i].site}</a>
                </div>
              </div>
          </div>
        ))
      }
      </StyledCarousel>
		</div>
	);
};