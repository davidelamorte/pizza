import { graphql, useStaticQuery } from 'gatsby';
import Img from 'gatsby-image';
import React from 'react';
import styled from 'styled-components';
import { Flex } from '../atoms/Flex';

const LogoWrapper = styled.div`
	width: 100px;
	@media (min-width: ${({ theme }) => theme.breakpoints[1]}) {
		width: 150px;
	}
`;

export const Clients = () => {
	const { allFile } = useStaticQuery(graphql`
		query {
			allFile(
				filter: {
					extension: { regex: "/(jpg)|(jpeg)|(png)/" }
					relativeDirectory: { eq: "clients" }
				}
			) {
				edges {
					node {
						id
						childImageSharp {
							fluid(grayscale: true) {
								...GatsbyImageSharpFluid_tracedSVG
							}
						}
					}
				}
			}
		}
	`);

	return (
		<Flex justify="space-between" align="center">
			{allFile.edges.map(image => (
				<LogoWrapper key={image.node.id}>
					<Img fluid={image.node.childImageSharp.fluid} alt="Client logo" />
				</LogoWrapper>
			))}
		</Flex>
	);
};
