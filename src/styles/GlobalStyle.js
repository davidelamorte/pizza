import { createGlobalStyle } from 'styled-components';
import Normalize from './Normalize';
import Typography from './Typography';

const GlobalStyle = createGlobalStyle`
	@import url('https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap');
	@import url('https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap');

	${Normalize}
	* {
		box-sizing: border-box;
	}
	body {
		overflow-x: hidden !important;
		background: ${props => props.theme.bodyBkg()};
		scroll-behavior: smooth;
		box-sizing: border-box;
		background-color: white;
	}
	${Typography}
	
	#clock {
		transform: rotate(-62deg);
	}
	
	header, .logo {
		transition: all 500ms ease-in-out;
	}
`;

export default GlobalStyle;
