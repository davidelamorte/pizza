import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

const LeftDiv = styled.div`
	width: 50%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-wrap: wrap;
	@media (max-width: 900px) {
    width: 100%;
  }
`;

const RightDiv = styled.div`
	width: 50%;
	height: 100%;
	@media (max-width: 900px) {
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
		text-align: center;
  }
`;

const PizzaImg = styled(Img)`
	width: 100%;
`;

const TextWrapper = styled.div`
	margin-top: 120px;
	max-width: 400px;
	line-height: 1.2;
	margin-left: 40px;
	@media (max-width: 900px) {
		margin: 0;
  }
`;

const Title = styled.h2`
	font-size: 46px;
	font-weight: 700;
	line-height: 57px;
	@media (max-width: 460px) {
		padding: 10px;
		font-size: 30px;
		line-height: 40px;
  }
`;

const Text = styled.div`
	font-size: 19px;
	line-height: 24px;
	@media (max-width: 460px) {
		padding: 10px;
		font-size: 15px;
		line-height: 30px;
  }
`;

export const SectionOne = () => {

	const data = useStaticQuery(graphql`
		query {
			pizza: file(relativePath: { eq: "section1.jpg" }) {
				childImageSharp {
					fluid {
						...GatsbyImageSharpFluid_tracedSVG
					}
				}
			}
		}
	`);

	console.log(data)

	return (
		<div
			css={`
				height: 100vh;
				display: flex;
				align-items: center;
				flex-wrap: wrap;
				background-color: white;
				@media (max-width: 900px) {
					height: auto;
				}
			`}
		>
			<LeftDiv>
				<PizzaImg fluid={data.pizza.childImageSharp.fluid}/>
			</LeftDiv>
			<RightDiv>
				<TextWrapper>
					<Title>
						Dimentica la pizza come l'hai sempre conosciuta
					</Title>
					<Text>
						Napoletana, romana, sottile, spessa, artigianale, industriale, gourmet, bio, etc. <br />
						Le categorie le conosci bene. <br />
					</Text>
					<Text css={`margin-top: 20px`}>
						Ora però puoi provare ad andare oltre gli schemi. <br />
						Allontanarti dalle consuetudini delle buone pizzerie. <br />
						Uscire dal mondo delle pizze “normali”.
					</Text>
				</TextWrapper>
			</RightDiv>
		</div>
	);
};