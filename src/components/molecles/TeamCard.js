import styled from 'styled-components';
import React from 'react';
import Img from 'gatsby-image';

const TeamCardContainer = styled.div`
	background-color: ${props => props.isDarkMode ? props.theme.colors.gray : props.theme.colors.light};
	box-shadow: ${props => props.theme.shadows.base};
`;

const TeamCardTextContainer = styled.div`
	display: flex;
	justify-content: center;
	flex-direction: column;
	padding: ${props => props.theme.spaces[1]};
`;

const TeamCardTitle = styled.div`
	font-size: 120%;
	font-weight: bold;
	text-transform: uppercase;
	color: ${props => props.theme.colors.primary};
`;

const TeamMemberSquare = styled.div`
	width: 10px;
	height: 10px;
	background-color: ${props => props.theme.colors.primary};
	margin-right: 10px;
`;

const TeamCardSubtitle = styled.div`
	color: ${props => props.theme.colors.lightGray};
	display: flex;
	justify-content: flex-start;
	align-items: center;
`;

export const TeamCard = props => (
	<TeamCardContainer isDarkMode={props.isDarkMode}>
		<Img fluid={props.image} alt={props.title} />
		<TeamCardTextContainer>
			<TeamCardTitle>{props.title}</TeamCardTitle>
			<TeamCardSubtitle>
				<TeamMemberSquare />
				{props.subtitle}
			</TeamCardSubtitle>
			<div>{props.email}</div>
			<div>{props.tel}</div>
		</TeamCardTextContainer>
	</TeamCardContainer>
);
