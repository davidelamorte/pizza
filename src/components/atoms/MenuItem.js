import React from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';

const StyledLink = styled.div`
	color: ${props => props.theme.colors.primary};
	text-transform: uppercase;
	font-weight: bold;
`;

const MenuItem = ({ children, to }) => (
	<StyledLink>
		<Link to={to}>{children}</Link>
	</StyledLink>
);

export default MenuItem;
