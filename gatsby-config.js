module.exports = {
	siteMetadata: {
		title: 'Patrick Ricci',
		description:
			'Un nuovo concetto di pizza',
		author: 'Slashfactory'
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		{
			resolve: 'gatsby-source-filesystem',
			options: {
				name: 'assets',
				path: `${__dirname}/src/assets`
			}
		},
		'gatsby-transformer-sharp',
		'gatsby-plugin-sharp',
		{
			resolve: 'gatsby-plugin-manifest',
			options: {
				name: 'Slashfactory',
				short_name: 'slashfactory',
				start_url: '/',
				background_color: '#FCFCFC',
				theme_color: '#ff404c',
				display: 'minimal-ui',
				icon: 'favicon.png' // This path is relative to the root of the site.
			}
		},
		'gatsby-plugin-styled-components'
		// this (optional) plugin enables Progressive Web App + Offline functionality
		// To learn more, visit: https://gatsby.dev/offline
		// 'gatsby-plugin-offline',
	]
};
