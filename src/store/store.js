const initialState = {
	isDarkMode: false
};

export const TOGGLE_DARK_MODE = 'TOGGLE_DARK_MODE';

export const handleDarkMode = payload => ({
	type: TOGGLE_DARK_MODE,
	payload
});

export default (state = initialState, action) => {
	switch (action.type) {
		case TOGGLE_DARK_MODE:
			return { ...state, isDarkMode: action.payload };
		default:
			return state;
	}
};
