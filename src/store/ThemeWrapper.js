import React, { useEffect } from 'react';
import themeDark from '../styles/themeDark';
import theme from '../styles/theme';
import { ThemeProvider } from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { TOGGLE_DARK_MODE } from './store';

const ThemeWrapper = ({ children }) => {
	const dispatch = useDispatch();
	const darkModeStore = useSelector(state => state.store.isDarkMode);

	// let isDarkMode;
	//
	useEffect(() => {
		if (!localStorage.getItem('isDarkMode')) {
			dispatch({ type: TOGGLE_DARK_MODE, payload: false });
			localStorage.setItem('isDarkMode', JSON.stringify(false));
		} else {
			dispatch({ type: TOGGLE_DARK_MODE, payload: JSON.parse(localStorage.getItem('isDarkMode')) });
		}

	}, []);


	return (
		<ThemeProvider theme={darkModeStore ? themeDark : theme}>
			{children}
		</ThemeProvider>
	);
};

export default ThemeWrapper;
