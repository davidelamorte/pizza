const theme = {
	breakpoints: ['40rem', '48rem', '64rem', '80rem'],
	shadows: {
		base: '0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)',
		md: '0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)',
		lg:
			'0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)',
		xl:
			'0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)'
	},
	colors: {
		primary: '#ff404c',
		dark: '#242423',
		gray: '#444242',
		lightGray: '#9B9696',
		light: '#FCFCFC',
		transparent: 'transparent',
		background: '#FCFCFC'
	},
	text: function() {
		return this.colors.dark;
	},
	headerBkg: function() {
		return this.colors.light;
	},
	bodyBkg: function() {
		return this.colors.light;
	},
	fontWeights: {
		thin: '200',
		normal: '400',
		bold: '600'
	},
	fontSizes: {
		small: 16,
		base: 18,
		large: 20
	},
	fonts: {
		sans: 'Source Sans Pro, sans-serif',
		serif: 'Playfair Display, serif',
		monospace: 'monospace',
		montserrat: 'Montserrat, sans-serif'
	},
	spaces: ['0', '1rem', '1.5rem', '2rem'],
	typographyConfig: {
		baseLineHeight: 1.75,
		hasIndentation: true,
		hasDropCap: true,
		ratio: 1.333,
		elegantHr: true,
		rhythm: function() {
			return this.baseLineHeight;
		}
	},
	grid: {
		columnGap: '2vw',
		rowGap: '2vh'
	}
};

export default theme;
