import styled from 'styled-components';
import Dots from '../../images/dots.jpeg';

export const Slash = styled.div`
	position: relative;
	top: 0;
	left: 0;
	right: 0;
	bottom: 0;
	background: transparent;
	@media (min-width: ${props => props.theme.breakpoints[2]}) {
		position: absolute;
		top: 0;
		left: 40%;
		width: 100vw;
		height: 100%;
		background: ${props => props.theme.colors.primary};
		z-index: -1;
		transform: skewX(-28deg);
	}
`;
