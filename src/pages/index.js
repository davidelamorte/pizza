import React from 'react';
import SEO from '../components/seo';
import GlobalStyle from '../styles/GlobalStyle';

import * as Sections from '../components/sections';

import { ParallaxProvider } from 'react-scroll-parallax';

const IndexPage = () => {

	return (
		<div
			css={`
				display: flex;
				flex-direction: column;
			`}
		>
			<SEO
				title="Patrick Ricci"
				description="Un nuovo concetto di pizza"
			/>
			<ParallaxProvider>
				<GlobalStyle />
				<Sections.Hero />
				<Sections.SectionOne />
				<Sections.SectionTwo />
				<Sections.SectionThree />
				<Sections.Awards />
				<Sections.CarouselSection />
				<Sections.SectionFour />
				<Sections.Footer />
			</ParallaxProvider>
		</div>
	);
};

export default IndexPage;