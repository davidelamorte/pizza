import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

const LeftDiv = styled.div`
	width: 50%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-wrap: wrap;
	@media (max-width: 900px) {
    width: 100%;
  }
`;

const RightDiv = styled.div`
	width: 50%;
	height: 100%;
	@media (max-width: 900px) {
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
		text-align: center;
  }
`;

const PatrickImg = styled(Img)`
	width: 100%;
	height: 100%;
`;

const TextWrapper = styled.div`
	margin-top: 120px;
	max-width: 500px;
	line-height: 1.2;
	margin-left: 100px;
	@media (max-width: 900px) {
		height: 100%;
		margin: 50px 0;
  }
`;

const Text = styled.div`
	font-size: 19px;
	line-height: 24px;
	@media (max-width: 460px) {
		font-size: 15px;
		padding: 10px;
  }
`;

const BoldText = styled.span`
	font-weight: bold;
`;

export const SectionFour = () => {

	const data = useStaticQuery(graphql`
		query {
			patrick: file(relativePath: { eq: "section4.jpg" }) {
				childImageSharp {
					fluid {
						...GatsbyImageSharpFluid_tracedSVG
					}
				}
			}
		}
	`);

	console.log(data)

	return (
		<div
			css={`
				height: 80vh;
				display: flex;
				flex-wrap: wrap;
				align-items: center;
				background-color: white;
				@media (max-width: 900px) {
					height: auto;
				}
			`}
		>
			<LeftDiv>
				<PatrickImg fluid={data.patrick.childImageSharp.fluid}/>
			</LeftDiv>
			<RightDiv>
				<TextWrapper>
					<Text>
						Nulla è casuale.
					</Text>
					<Text css={`margin-top: 20px`}>
						<BoldText>Sono scelte precise,</BoldText> prese dopo anni di ricerche e sperimentazioni <BoldText>cercando di esaltare il valore della pizza,</BoldText> nel rispetto del cibo e di te che la mangerai. 
					</Text>
					<Text css={`margin-top: 20px`}>
						<BoldText>Scarica ora</BoldText> il menu e gli approfondimenti: capirai veramente cosa ho scoperto, <BoldText>perchè e come ho deciso di proporti un'esperienza differente dalla classica pizza.</BoldText>
					</Text>
				</TextWrapper>
			</RightDiv>
		</div>
	);
};