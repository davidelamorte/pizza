import styled from 'styled-components';

const compoundsColumns = {
	4: '2fr 1fr 1fr 2fr',
	6: '3fr 1fr 2fr 2fr 1fr 3fr',
	8: '6fr 1fr 4fr 3fr 3fr 4fr 1fr 6fr'
};

export const CompoundGrid = styled.div`
	height: 100%;
	display: grid;
	grid-template-columns: 1fr;
	grid-column-gap: ${({ theme }) => theme.grid.columnGap};
	grid-row-gap: ${({ theme }) => theme.grid.rowGap};
	align-content: ${props => props.align || 'stretch'};
	@media (min-width: ${props => props.theme.breakpoints[2]}) {
		grid-template-columns: ${props => compoundsColumns[props.type]};
	}
`;

export const Grid = styled.div`
	height: 100%;
	display: grid;
	grid-template-columns: 1fr;
	grid-column-gap: ${({ theme }) => theme.grid.columnGap};
	grid-row-gap: ${({ theme }) => theme.grid.rowGap};
	align-content: ${props => props.align || 'stretch'};
	@media (min-width: ${props => props.theme.breakpoints[2]}) {
		grid-template-columns: repeat(${props => props.columnNumber || 12}, 1fr);
	}
`;

export const Gallery = styled.div`
	height: 100%;
	display: grid;
	grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
	grid-template-rows: auto;
	grid-column-gap: ${({ theme }) => theme.grid.columnGap};
	grid-row-gap: ${({ theme }) => theme.grid.rowGap};
	align-items: center;
`;

export const GridSpan = styled.div`
	width: 100%;
	height: 100%;
	@media (min-width: ${props => props.theme.breakpoints[2]}) {
		grid-column: ${props => props.gridColumn};
		grid-row: ${props => props.gridRow};
		align-content: ${props => props.align || 'stretch'};
	}
`;
