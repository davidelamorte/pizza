import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

const AwardImg = styled.img`
  width: 150px;
  margin: 15px;
`;

const ImgContainer = styled.div`
  width: 150px;
  margin: 15px;
`;

export const Awards = () => {  

	const data = useStaticQuery(graphql`
		query {
      allFile(filter: {extension: {regex: "/(jpg)|(png)|(jpeg)/"}, relativeDirectory: {eq: "awards"}}) {
        edges {
          node {
            childImageSharp {
              fixed {
                src
              }
            }
          }
        }
      }
    }
	`);

	console.log('aw', data.allFile.edges);

	return (
		<div
			css={`
        height: auto;
        display: flex;
        flex-wrap: wrap;
        align-items: center;
        background-color: white;
        padding: 50px;
        justify-content: center;
        @media (max-width: 1000px) {
          height: auto;
          padding: 20px;
          justify-content: space-around;
				}
			`}
		>
      { data &&
        data.allFile.edges.map((el, i) => (
          <ImgContainer>
				    <AwardImg key={i} src={el.node.childImageSharp.fixed.src}/>
          </ImgContainer>
        ))
      }
		</div>
	);
};