import styled from 'styled-components';

const Button = styled.button`
	background: none;
	cursor: pointer;
	padding: 25px 80px;
	text-transform: uppercase;
	font-weight: 600;
	outline: none;
	position: relative;
	transition: all 0.3s;
	border: 3px solid ${props => props.theme.colors.primary};
	color: ${props => props.theme.colors.primary};
	overflow: hidden;
	width: 100%;
	@media (min-width: ${props => props.theme.breakpoints[2]}) {
		width: auto;
		border: 3px solid ${props => props.theme.colors.light};
		color: ${props => props.theme.colors.light};
	}
	&:focus {
		outline: 0 !important;
	}
	&:after {
		content: '';
		position: absolute;
		z-index: -1;
		width: 100%;
		height: 0;
		top: 50%;
		left: 50%;
		background: ${props => props.theme.colors.primary};
		opacity: 0;
		transition: all 0.3s;
		transform: translateX(-50%) translateY(-50%) rotate(-63deg);
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			background: ${props => props.theme.colors.light};
		}
	}
	&:hover {
		color: ${props => props.theme.colors.light};
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			color: ${props => props.theme.colors.primary};
		}
		&:after {
			height: 260%;
			opacity: 1;
		}
	}
	&:active {
		&:after {
			height: 400%;
		}
	}
`;

export default Button;
