import styled from 'styled-components';

export const Flex = styled.div`
	height: 100%;
	display: flex;
	justify-content: ${props => props.justify || 'flex-start'};
	align-items: ${props => props.align || 'flex-start'};
	flex-wrap: wrap;
`;
