import { library } from '@fortawesome/fontawesome-svg-core';
import { faAdjust } from '@fortawesome/free-solid-svg-icons/faAdjust';
import 'animate.css';
import AOS from 'aos';
import 'aos/dist/aos.css';
import PropTypes from 'prop-types';
import React, { useEffect } from 'react';
import GlobalStyle from '../styles/GlobalStyle';
import Header from './molecles/Header';

library.add(faAdjust);

const Layout = ({ children }) => {
	useEffect(() => {
		AOS.init();
	}, []);

	return (
		<>
			<GlobalStyle />
			<Header />
			<main>{children}</main>
		</>
	);
};

Layout.propTypes = {
	children: PropTypes.node.isRequired
};

export default Layout;
