import { css } from 'styled-components';

const Typography = css`
	html,
	body {
		font-family: ${({ theme }) => theme.fonts.montserrat};
		color: ${({ theme }) => theme.text()};
		word-wrap: break-word;
		-webkit-font-smoothing: subpixel-antialiased;
		text-rendering: optimizeLegibility;
		line-height: ${({ theme }) => theme.typographyConfig.baseLineHeight};
		font-size: ${({ theme }) => theme.fontSizes.small}px;
		font-weight: ${({ theme }) => theme.fontWeights.normal};
		@media (min-width: ${({ theme }) => theme.breakpoints[1]}) {
			font-size: ${({ theme }) => theme.fontSizes.base}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[2]}) {
			font-size: ${({ theme }) => theme.fontSizes.large};
		}
	}
	p {
		hyphens: ${({ theme }) => theme.hyphens && 'auto'};
		margin-bottom: ${({ theme }) =>
			theme.typographyConfig.hasIndentation
				? 0
				: theme.typographyConfig.rhythm() + 0.25}rem;
		& + p {
			${({ theme }) =>
				theme.typographyConfig.hasIndentation
					? `text-indent: 2rem; margin-bottom: 0;`
					: `margin-bottom: ${theme.typographyConfig.rhythm() + 0.25}rem`}
		}
		${({ theme }) =>
			theme.typographyConfig.hasDropCap &&
			`
			&:nth-of-type(1)::first-letter {
				float: left;
				margin: 0.08em 0.08em 0 0;
				line-height: 0.7;
				background: transparent;
				color: ${theme.colors.primary};
				font-size: ${theme.fontSizes.small *
					Math.pow(theme.typographyConfig.ratio, 6)}px;
				@media (min-width: ${theme.breakpoints[2]}) {
					font-size: ${theme.fontSizes.base *
						Math.pow(theme.typographyConfig.ratio, 6)}px;
				}
				@media (min-width: ${theme.breakpoints[3]}) {
					font-size: ${theme.fontSizes.large *
						Math.pow(theme.typographyConfig.ratio, 6)}px;
				}
			}
		`}
	}
	h1,
	.h1 {
		font-size: ${({ theme }) =>
			theme.fontSizes.small * Math.pow(theme.typographyConfig.ratio, 5)}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.base * Math.pow(theme.typographyConfig.ratio, 5)}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.large * Math.pow(theme.typographyConfig.ratio, 5)}px;
		}
	}
	h2,
	.h2 {
		font-size: ${({ theme }) =>
			theme.fontSizes.small * Math.pow(theme.typographyConfig.ratio, 4)}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.base * Math.pow(theme.typographyConfig.ratio, 4)}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.large * Math.pow(theme.typographyConfig.ratio, 4)}px;
		}
	}
	h3,
	.h3 {
		font-size: ${({ theme }) =>
			theme.fontSizes.small * Math.pow(theme.typographyConfig.ratio, 3)}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.base * Math.pow(theme.typographyConfig.ratio, 3)}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.large * Math.pow(theme.typographyConfig.ratio, 3)}px;
		}
	}
	h4,
	.h4 {
		font-size: ${({ theme }) =>
			theme.fontSizes.small * Math.pow(theme.typographyConfig.ratio, 2)}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.base * Math.pow(theme.typographyConfig.ratio, 2)}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.large * Math.pow(theme.typographyConfig.ratio, 2)}px;
		}
	}
	h5,
	.h5 {
		font-size: ${({ theme }) =>
			theme.fontSizes.small * theme.typographyConfig.ratio}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.base * theme.typographyConfig.ratio}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) =>
				theme.fontSizes.large * theme.typographyConfig.ratio}px;
		}
	}
	h6,
	.h6 {
		font-size: ${({ theme }) => theme.fontSizes.small}px;
		@media (min-width: ${props => props.theme.breakpoints[2]}) {
			font-size: ${({ theme }) => theme.fontSizes.base}px;
		}
		@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
			font-size: ${({ theme }) => theme.fontSizes.large}px;
		}
	}
	h1,
	h2,
	h3,
	h4,
	h5,
	h6,
	.h1,
	.h2,
	.h3,
	.h4,
	.h5,
	.h6 {
		font-weight: ${({ theme }) => theme.fontWeights.bold};
		text-rendering: optimizeLegibility;
		line-height: ${({ theme }) => theme.typographyConfig.baseLineHeight - 0.75};
		margin-bottom: ${({ theme }) => theme.typographyConfig.rhythm() + 0.25}rem;
		margin-top: ${({ theme }) => theme.typographyConfig.rhythm() + 0.75}rem;
	}
	small {
		font-size: 0.75rem;
	}
	ul,
	ol {
		list-style-position: outside;
	}
	ul {
		list-style-type: disc;
	}
	ol {
		list-style-type: decimal;
	}
	code {
		font-size: 0.85rem;
	}
	pre {
		margin: ${({ theme }) => theme.typographyConfig.baseLineHeight}rem 0;
		white-space: normal;
	}
	table {
		width: 100%;
		margin-bottom: ${({ theme }) => theme.typographyConfig.rhythm() + 0.25}rem;
		th,
		td {
			padding: 0.5rem 1rem;
		}
		thead {
			color: ${({ theme }) => theme.colors.light};
			background-color: ${({ theme }) => theme.colors.gray};
			th {
				text-align: left;
				font-weight: normal;
			}
		}
		tbody {
			tr {
				&:nth-child(odd) {
					background-color: transparent;
				}
				&:nth-child(even) {
					background-color: ${({ theme }) => theme.colors.lightGray};
				}
			}
		}
	}
	kbd,
	samp {
		font-size: 0.85rem;
	}
	abbr[title] {
		border-bottom: 1px dotted;
		cursor: help;
		text-decoration: none;
	}
	hgroup,
	figure,
	ul,
	ol,
	table,
	fieldset,
	form,
	noscript,
	iframe,
	address,
	dl,
	dd,
	blockquote,
	small {
		margin-bottom: ${({ theme }) => theme.typographyConfig.rhythm() + 0.25}rem;
	}
	blockquote {
		color: ${({ theme }) => theme.colors.gray};
		margin-left: ${({ theme }) => theme.typographyConfig.rhythm()}rem;
		margin-right: ${({ theme }) => theme.typographyConfig.rhythm()}rem;
		font-style: italic;
		border-left: 0.2rem solid ${({ theme }) => theme.colors.lightGray};
		padding-left: 1rem;
	}
	hr {
		${({ theme }) =>
			theme.typographyConfig.elegantHr
				? `
				border: 0;
				background: ${theme.colors.primary};
				height: 3px;
				margin: ${theme.typographyConfig.rhythm()}rem auto;
				text-align: center;
				width: 20%;
				@media (min-width: ${theme.breakpoints[1]}) {
					width: 10%;
				}
				&:after {
					content: '§';
					display: inline-block;
					position: relative;
					top: -0.80em;
					padding: 0 0.25em;
					background: ${theme.colors.light};
					color: ${theme.colors.dark};
				}
			}`
				: ` 
				margin: calc(${theme.typographyConfig.rhythm()}rem - 1px) 0;
				background-color: ${theme.colors.gray};
				border: none;
				height: 1px;
		 `}
		}
	}
`;

export default Typography;
