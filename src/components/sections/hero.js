import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';
import { useSelector } from 'react-redux';

import video from '../../assets/hero-video.mp4';

const Video = styled.video`
	position: absolute;
	top: 0;
	left: 0;
	width: 100vw;
	z-index: -100;
`;

const Logo = styled(Img)`
	position: absolute !important;
	top: 70px;
	left: 50%;
	width: 350px;
	height: 82px;
	transform: translateX(-50%);
	@media (max-width: 900px) {
		width: 50px;
	}
`;

const TextWrapper = styled.div`
	margin-top: 120px;
	max-width: 700px;
	line-height: 1.2;
	@media (max-width: 500px) {
		display: none;
	}
`;

const UpperText = styled.div`
	width: 100%;
	color: white;
	font-size: 30px;
	text-align: center;
	font-style: oblique;
	margin-bottom: 20px;
	@media (max-width: 950px) {
		font-size: 15px;
	}
`;

const MainText = styled.div`
	width: 100%;
	color: white;
	font-size: 74px;
	text-align: center;
	@media (max-width: 950px) {
		font-size: 30px;
	}
`;

export const Hero = () => {

	const data = useStaticQuery(graphql`
		query {
			logo: file(relativePath: { eq: "logo.png" }) {
				childImageSharp {
					fixed(width: 350) {
						...GatsbyImageSharpFixed_tracedSVG
					}
				}
			}
		}
	`);

	console.log('data', data)

	return (
		<div
			css={`
				position: relative;
				height: 700px;
				display: flex;
				justify-content: center;
				align-items: center;
				overflow: hidden;
				@media (max-width: 900px) {
					height: 300px;
				}
				@media (max-width: 500px) {
					height: 400px;
				}
				@media (max-width: 500px) {
					height: 300px;
				}
			`}
		>
			<Video src={video} muted autoPlay loop />
			<Logo fixed={data.logo.childImageSharp.fixed} />
			<TextWrapper>
				<UpperText>Scopri</UpperText>
				<MainText>un nuovo concetto di pizza</MainText>
			</TextWrapper>
		</div>
	);
};