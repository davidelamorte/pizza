import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

const StyledImg = styled(Img)`
  margin-bottom: 15px;
`;

const Logo = styled(Img)`
`;

const TextWrapper = styled.div`
  text-align: center;
  line-height: 2;
  color: white;
`;

const Text = styled.div`
	font-size: 15px;
  line-height: 27px;
  color: white;
  max-width: 200px;
  text-align: center;
`;

const TopDiv = styled.div`
  display: flex;
  flex-wrap: wrap;
  background-color: black;
  height: 90%;
  width: 100%;
  @media (max-width: 900px) {
    height: auto;
    align-items: center;
    justify-content: center;
  }
`;

const BottomDiv = styled.div`
  background-color: white;
  height: 10%;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  justify-content: center;
  @media (max-width: 900px) {
    text-align: center;
    height: auto;
    align-items: center;
    justify-content: center;
    padding: 10px;
  }
`;

const LeftDiv = styled.div`
  position: relative;
  height: 100%;
  width: 33%;
  display: flex;
  align-items: center;
  justify-content: center
  @media (max-width: 900px) {
    width: 100%;
  }
`;

const CenterDiv = styled.div`
  height: 100%;
  width: 33%;
  display: flex;
  align-items: center;
  justify-content: center;
  @media (max-width: 900px) {
    width: 100%;
    margin-bottom: 20px;
    margin-top: 20px;
  }
`;

const RightDiv = styled.div`
  height: 100%;
  width: 33%;
  display: flex;
  align-items: center;
  justify-content: center;
  @media (max-width: 900px) {
    width: 100%;
    margin-bottom: 20px;
  }
`;

const Link = styled.a`
  color: white;
  text-decoration: underline;
	font-size: 15px;
`;


export const Footer = () => {

	const data = useStaticQuery(graphql`
    query {
      logo: file(relativePath: { eq: "logo.png" }) {
        childImageSharp {
          fixed(width: 300) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      },
      whats: file(relativePath: { eq: "whats.png" }) {
        childImageSharp {
          fixed(width: 42) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      },
      gmap: file(relativePath: { eq: "gmap.png" }) {
        childImageSharp {
          fixed(width: 42) {
            ...GatsbyImageSharpFixed_tracedSVG
          }
        }
      }
    }
	`);

	console.log(data)

	return (
		<div
			css={`
        height: 80vh;
        @media (max-width: 900px) {
          height: auto;
        }
			`}
		>
      <TopDiv>
        <LeftDiv>
          <div css={`
            width: 100%;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            @media (max-width: 900px) {
              margin-top: 20px;
            }
          }
			    `}>
          <StyledImg fixed={data.whats.childImageSharp.fixed} />
          <Text>
            Tel. e WhatsApp +390118973883 <br />
            clicca qui per chattare ora <br />
            (rispondiamo dalle 15.30 <br />
            lunedì escluso), <br /> <br />
            ciao@patrickricci.it
          </Text>
          </div>
        </LeftDiv>
        <CenterDiv>
          <Logo fixed={data.logo.childImageSharp.fixed} />
        </CenterDiv>
        <RightDiv css={`
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
			    `}>
          <TextWrapper>
          <Text>
            Martedì - Domenica <br />
            19:30 - 23:00 <br />
            ultimo servizio 22:30 <br />
            Lunedì chiuso
          </Text>
          <Text style={{marginTop: '20px'}}>
            Via Martiri della Libertà <br />
            103 10099 | San Mauro Torinese | <br />
            ITA
          </Text>
          </TextWrapper> 
          <StyledImg style={{marginTop: '20px'}} fixed={data.gmap.childImageSharp.fixed} />
          <Link href='https://www.google.com/maps/place/Patrick+Ricci+Terra+Grani+Esplorazioni+%22ex+Pomodoro+e+Basilico%22/@45.0982684,7.7545934,16z/data=!4m5!3m4!1s0x478873dad04a3fcd:0x75309d0c1b0f3b3!8m2!3d45.1016981!4d7.7649026'>
            Guarda su Gmap
          </Link>
        </RightDiv>
      </TopDiv>
      <BottomDiv>
        <span
          css={`
            margin-right: 50px;
            @media (max-width: 460px) {
              margin: 0;
            }
          `}>
            © 2019 Venticinque s.rl.s. P.IVA: 11362430016
        </span>
        <Link href='https://www.iubenda.com/privacy-policy/69202220'
          style={{color: 'black', marginRight: '20px'}}>
            Privacy policy
        </Link>
        <Link href='https://www.iubenda.com/privacy-policy/69202220/cookie-policy'
          style={{color: 'black'}}>
            Cookie policy
        </Link>
      </BottomDiv>
		</div>
	);
};