import React from 'react';
import styled from 'styled-components';

const StyledSection = styled.section`
	height: 100vh;
	width: 100%;
`;

export const Section = ({ className, children }) => (
	<div className={className}>
		<StyledSection>{children}</StyledSection>
	</div>
);
