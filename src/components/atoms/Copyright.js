import React from 'react';
import styled from 'styled-components';

const StyledCopy = styled.div`
	text-align: center;
	color: ${props => props.theme.colors.lightGray};
`;

export const Copyright = props => {
	const date = new Date();
	const year = date.getFullYear();
	return (
		<StyledCopy>
			Copyright © {year} Slashfactory
		</StyledCopy>
	)
};