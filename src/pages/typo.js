import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';

const TypoPage = () => (
	<Layout>
		<SEO title="Typography" />
		<h1>Hi from the first page</h1>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor pariatur,
			reprehenderit? Adipisci aliquid amet aperiam aspernatur commodi cumque
			dicta dolores dolorum ducimus ea et eum excepturi, facere hic incidunt
			ipsa iste iusto magnam minus molestiae molestias nesciunt nobis numquam
			officia omnis placeat quam qui quo quos rem reprehenderit rerum sapiente,
			suscipit tempora temporibus vel voluptatibus. Amet commodi, consequatur,
			earum esse explicabo inventore minima mollitia necessitatibus porro rem
			sed soluta suscipit tempora? Commodi cupiditate eos inventore pariatur
			provident? Aliquam enim omnis provident temporibus ullam. Ad architecto,
			atque eos error est ex excepturi explicabo, id impedit inventore optio
			perspiciatis quis saepe sint!
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem culpa
			cupiditate dolorum enim facilis iusto libero magnam maiores nobis
			praesentium repellat repellendus, suscipit unde, velit veniam! A aperiam
			autem labore modi porro. Ad aspernatur, beatae dignissimos error eveniet
			exercitationem fugit ipsam minus natus obcaecati officiis quas quod sit,
			veritatis vero?
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem culpa
			cupiditate dolorum enim facilis iusto libero magnam maiores nobis
			praesentium repellat repellendus, suscipit unde, velit veniam! A aperiam
			autem labore modi porro. Ad aspernatur, beatae dignissimos error eveniet
			exercitationem fugit ipsam minus natus obcaecati officiis quas quod sit,
			veritatis vero?
		</p>
		<h2>I'm an h2 title</h2>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Autem culpa
			cupiditate dolorum enim facilis iusto libero magnam maiores nobis
			praesentium repellat repellendus, suscipit unde, velit veniam! A aperiam
			autem labore modi porro. Ad aspernatur, beatae dignissimos error eveniet
			exercitationem fugit ipsam minus natus obcaecati officiis quas quod sit,
			veritatis vero?
		</p>
		<pre>
			<code>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illum,
				quaerat.
			</code>
		</pre>
		<blockquote>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, ex hic illo
			neque nulla provident reprehenderit saepe sed! Et inventore ipsam non.
			Dolore facere, hic iusto nisi qui quo temporibus!
		</blockquote>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores
			provident quo quod? Ea earum est inventore molestias obcaecati sint? A
			assumenda cum deserunt enim eos exercitationem id, nesciunt nobis ullam
			unde vero voluptate, voluptatibus voluptatum! Accusamus doloribus ea
			magnam nesciunt odio praesentium vel. Accusantium architecto dolorum ea
			exercitationem facilis rem?
		</p>
		<hr />
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam
			doloremque ea ipsam itaque libero mollitia qui veritatis vitae. A commodi
			consequuntur dolore enim eos error, ex expedita fugit impedit mollitia
			nulla omnis, pariatur porro reiciendis rem repellat totam ullam, veniam.
			Please press <kbd>Shift + 4</kbd>. Io sono{' '}
			<abbr title="Abbreviazione">abbr</abbr> e io sono <samp>samp output</samp>{' '}
			and <small>I'm very small!</small>
		</p>
		<ul>
			<li>ciao</li>
			<li>ciao</li>
			<li>ciao</li>
		</ul>
		<ol>
			<li>ciao</li>
			<li>ciao</li>
			<li>ciao</li>
		</ol>
		<table>
			<thead>
				<tr>
					<th>Name</th>
					<th>Surname</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Alessandro</td>
					<td>Arcidiaco</td>
					<td>arcidiaco.a@gmail.com</td>
				</tr>
				<tr>
					<td>Daniele</td>
					<td>Gilio</td>
					<td>dany@slashfactory.it</td>
				</tr>
			</tbody>
		</table>
	</Layout>
);

export default TypoPage;
