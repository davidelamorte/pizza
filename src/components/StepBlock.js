import React from 'react';

const StepBlock = ({ text, image }) => {
	return (
		<div
			data-aos="zoom-in"
			data-aos-duration="500"
			className="text-center h-24"
		>
			<div className="text-large font-bold mb-2">{text}</div>
			<img src={image} alt={text} className="h-full mx-auto" />
		</div>
	);
};

export default StepBlock;
