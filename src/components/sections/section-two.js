import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';
import { Parallax } from 'react-scroll-parallax';

const RightDiv = styled.div`
	width: 50%;
	height: 100%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-wrap: wrap;
	padding-right: 50px;
  @media (max-width: 1000px) {
		width: 100%;
		padding: 0;
  }
`;

const LeftDiv = styled.div`
	width: 50%;
	height: 100%;
	padding-left: 50px;
  @media (max-width: 1000px) {
		width: 100%;
		display: flex;
		align-items: center;
		justify-content: center;
		text-align: center;
		padding: 0;
  }
`;

const PizzaImg = styled(Img)`
	width: 100%;
`;

const TextWrapper = styled.div`
	margin-top: 120px;
	max-width: 500px;
	line-height: 1.2;
	margin-left: 40px;
	@media (max-width: 900px) {
		margin: 0;
  }
`;

const Title = styled.h2`
	font-size: 46px;
	font-weight: 700;
	line-height: 57px;
	@media (max-width: 460px) {
		font-size: 30px;
		line-height: 40px;
		padding: 10px;
  }
`;

const Text = styled.div`
	font-size: 19px;
	line-height: 24px;
	@media (max-width: 460px) {
		font-size: 15px;
		line-height: 30px;
		padding: 10px;
  }
`;

export const SectionTwo = () => {

	const data = useStaticQuery(graphql`
		query {
			pizza: file(relativePath: { eq: "section2.jpg" }) {
				childImageSharp {
					fluid {
						...GatsbyImageSharpFluid_tracedSVG
					}
				}
			}
		}
	`);

	console.log(data)

	return (
		<Parallax className="custom-class" x={[-30, 30]} tagOuter="figure">
		<div
			css={`
				height: 100vh;
				display: flex;
				flex-wrap: wrap;
				align-items: center;
				background-color: white;
				@media (max-width: 1000px) {
					height: auto;
				}
			`}
		>
			<LeftDiv>
				<TextWrapper>
					<Title>
						Prova un'esperienza imprevedibile
					</Title>
					<Text>
						Nessuna ricetta classica. <br />
						In un solo boccone assapori abbinamenti imprevedibili tra più farine contemporaneamente e ingredienti di piccole eccellenze agricole. <br />
						(Quasi) Tutto preparato fuori cottura, uguale in ogni fetta.
					</Text>
					<Text css={`margin-top: 20px`}>
						...E non hai posate, mangi con le mani.
					</Text>
				</TextWrapper>
			</LeftDiv>
			<RightDiv>
				<PizzaImg fluid={data.pizza.childImageSharp.fluid}/>
			</RightDiv>
		</div>
		</Parallax>
	);
};