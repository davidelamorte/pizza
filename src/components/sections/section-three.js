import { graphql, useStaticQuery } from 'gatsby';
import styled from 'styled-components';

import Img from 'gatsby-image';
import React from 'react';

import { Parallax } from 'react-scroll-parallax';

const TopDiv = styled.div`
	width: 100%;
	height: 40%;
`;


const BottomDiv = styled.div`
	width: 100%;
	height: 60%;
	display: flex;
	align-items: center;
	justify-content: center;
	flex-wrap: wrap;
`;

const PizzaImg = styled(Img)`
	width: 600px;
`;

const TextWrapper = styled.div`
	color: white;
	margin-top: 80px;
	line-height: 1.2;
	display: flex;
	flex-wrap: wrap;
	justify-content: center;
`;

const Title = styled.h2`
	width: 100%;
	font-size: 46px;
	font-weight: 700;
	line-height: 57px;
	text-align: center;
	@media (max-width: 460px) {
		font-size: 30px;
		line-height: 40px;
  }
`;

const Text = styled.div`
	width: 100%;
	font-size: 19px;
	line-height: 24px;
	text-align: center;
	@media (max-width: 460px) {
		font-size: 15px;
		line-height: 30px;
  }
`;

export const SectionThree = () => {

	const data = useStaticQuery(graphql`
		query {
			pizza: file(relativePath: { eq: "section3.jpg" }) {
				childImageSharp {
					fixed(width: 1200) {
						...GatsbyImageSharpFixed_tracedSVG
					}
				}
			}
		}
	`);

	console.log(data)

	return (
		<div
			css={`
				height: 100vh;
				display: flex;
				flex-direction: column;
				align-items: center;
				justify-content: center;
				background-color: black;
				overflow: hidden;
			`}
		>
			<TopDiv>
				<TextWrapper>
					<Title>
						Esci dagli schemi
					</Title>
					<Text>
						E' un nuovo modo di concepire la pizza. <br />
						Né migliore, né peggiore. <br />
						Solo diverso.
					</Text>
				</TextWrapper>
			</TopDiv>
			<Parallax className="custom-class" x={[30, -30]} tagOuter="figure">
			<BottomDiv>
				<PizzaImg fixed={data.pizza.childImageSharp.fixed}/>
			</BottomDiv>
			</Parallax>
		</div>
	);
};