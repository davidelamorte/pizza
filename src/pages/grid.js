import React from 'react';
import Layout from '../components/layout';
import SEO from '../components/seo';

const GridPage = () => (
	<Layout>
		<SEO title="Grid" />
		<h1>Grid</h1>
		<h2>Classic grids</h2>
		<div className="grid-12 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
			<div className="bg-primary text-center h-16">5</div>
			<div className="bg-primary text-center h-16">6</div>
			<div className="bg-primary text-center h-16">7</div>
			<div className="bg-primary text-center h-16">8</div>
			<div className="bg-primary text-center h-16">9</div>
			<div className="bg-primary text-center h-16">10</div>
			<div className="bg-primary text-center h-16">11</div>
			<div className="bg-primary text-center h-16">12</div>
		</div>
		<div className="grid-6 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
			<div className="bg-primary text-center h-16">5</div>
			<div className="bg-primary text-center h-16">6</div>
		</div>
		<div className="grid-4 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
		</div>
		<h2>Compound grids</h2>
		<div className="compound-4 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
		</div>
		<div className="compound-6 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
			<div className="bg-primary text-center h-16">5</div>
			<div className="bg-primary text-center h-16">6</div>
		</div>
		<div className="compound-8 mb-8">
			<div className="bg-primary text-center h-16">1</div>
			<div className="bg-primary text-center h-16">2</div>
			<div className="bg-primary text-center h-16">3</div>
			<div className="bg-primary text-center h-16">4</div>
			<div className="bg-primary text-center h-16">5</div>
			<div className="bg-primary text-center h-16">6</div>
			<div className="bg-primary text-center h-16">7</div>
			<div className="bg-primary text-center h-16">8</div>
		</div>
	</Layout>
);

export default GridPage;
