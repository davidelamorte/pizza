import styled from 'styled-components';

export const Container = styled.div`
	height: 100%;
	width: 100%;
	padding: 0 ${({ theme }) => theme.spaces[1]};
	@media (min-width: ${({ theme }) => theme.breakpoints[1]}) {
		max-width: 768px;
		margin: 0 auto;
	}
	@media (min-width: ${({ theme }) => theme.breakpoints[2]}) {
		max-width: 1024px;
		margin: 0 auto;
	}
	@media (min-width: ${({ theme }) => theme.breakpoints[3]}) {
		max-width: 1280px;
		margin: 0 auto;
	}
`;
