import React from 'react';
import { navigate } from 'gatsby';
import SEO from '../components/seo';

const NotFoundPage = () => {
	const goToHome = () => {
		navigate('/');
	};
	return (
		<>
			<SEO title="404: Not found" />
			<h1>Ops! La pagina non esiste</h1>
			<button onClick={() => goToHome()}>Torna alla home</button>
		</>
	);
};

export default NotFoundPage;
