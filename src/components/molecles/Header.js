import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { graphql, Link, useStaticQuery } from 'gatsby';
import Img from 'gatsby-image';
import React, { useContext, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import styled, { ThemeContext } from 'styled-components';
import { handleDarkMode } from '../../store/store';
import { Container } from '../atoms/Container';
import { Flex } from '../atoms/Flex';

export const StyledHeader = styled.header`
	width: 100%;
	height: ${props => props.height};
	position: ${props => props.position || 'static'};
	background-color: ${props => props.bkg || props.theme.headerBkg()};
	box-shadow: ${props =>
		props.shadow ? props.theme.shadows[props.shadow] : 'none'};
	${props =>
		props.position === 'fixed' &&
		`
		top: 0;
		left: 0;
		right: 0;
		z-index: 10;
	`}
`;

const Header = props => {
	const themeContext = useContext(ThemeContext);
	const [isVisibleHeader, toggleShow] = useState(false);

	const data = useStaticQuery(graphql`
		query {
			logo: file(relativePath: { eq: "logo.png" }) {
				childImageSharp {
					fixed(width: 250) {
						...GatsbyImageSharpFixed_tracedSVG
					}
				}
			}
			logoWhite: file(relativePath: { eq: "logo_white.png" }) {
				childImageSharp {
					fixed(width: 250) {
						...GatsbyImageSharpFixed_tracedSVG
					}
				}
			}
		}
	`);

	const scrollFunction = () => {
		if (
			document.body.scrollTop > 50 ||
			document.documentElement.scrollTop > 50
		) {
			toggleShow(true);
		} else {
			toggleShow(false);
		}
	};

	useEffect(() => {
		window.addEventListener('scroll', scrollFunction);
		return () => window.removeEventListener('scroll', scrollFunction);
	}, []);

	const handleClickDarkMode = () => {
		localStorage.setItem('isDarkMode', JSON.stringify(!props.isDarkMode));
		props.handleDarkMode(!props.isDarkMode);
	};

	return (
		<StyledHeader
			bkg={isVisibleHeader ? themeContext.headerBkg() : 'transparent'}
			shadow={isVisibleHeader && 'base'}
			position="fixed"
			height="64px"
		>
			<Container>
				<Flex align="center" justify="space-between">
					<Link to="/">
						<Img
							fixed={
								props.isDarkMode
									? data.logoWhite.childImageSharp.fixed
									: data.logo.childImageSharp.fixed
							}
							alt="Slashfactory logo"
						/>
					</Link>
					{/*<Menu />*/}
					<Flex align="center">
						<div
							style={{ cursor: 'pointer' }}
							onClick={() => handleClickDarkMode()}
						>
							<FontAwesomeIcon
								icon="adjust"
								style={{
									color: props.isDarkMode ? 'white' : 'black',
									marginLeft: 20
								}}
							/>
						</div>
					</Flex>
				</Flex>
			</Container>
		</StyledHeader>
	);
};

const mapStateToProps = state => {
	return {
		isDarkMode: state.store.isDarkMode
	};
};

const mapDispatchToProps = dispatch => {
	return {
		handleDarkMode: v => dispatch(handleDarkMode(v))
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Header);
