import React from 'react';
import { Provider } from 'react-redux';
import { createStore as reduxCreateStore } from 'redux';
import rootReducer from '.';
import ThemeWrapper from './ThemeWrapper';

const { NODE_ENV } = process.env;

const createStore = () =>
	reduxCreateStore(
		rootReducer,
		NODE_ENV === 'development' &&
			window.__REDUX_DEVTOOLS_EXTENSION__ &&
			window.__REDUX_DEVTOOLS_EXTENSION__()
	);

export default ({ element }) => {


	return (
		<Provider store={createStore()}>
			<ThemeWrapper>
				{element}
			</ThemeWrapper>
		</Provider>
	);
};
