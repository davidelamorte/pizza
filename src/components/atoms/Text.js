import styled from 'styled-components';

// const Text = styled.span`
// 	/* color: ${props =>
// 		props.isDarkMode ? props.theme.colors.dark : props.theme.colors.light}
// 		|| ${props => props.theme.colors.dark}; */
// 	color: ${props => props.theme.colors.dark};
// 	@media (min-width: ${props => props.theme.breakpoints[2]}) {
// 		color: ${props =>
// 			props.isDarkMode ? props.theme.colors.dark : props.theme.colors.light};
// 	}
// `;

const Text = styled.span`
	color: ${props => props.theme.colors.dark};
`;

export default Text;
